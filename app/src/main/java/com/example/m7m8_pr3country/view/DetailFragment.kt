package com.example.m7m8_pr3country.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.View.INVISIBLE
import android.view.View.VISIBLE
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.lifecycleScope
import com.example.m7m8_pr3country.dataBase.CountryApplication
import com.example.m7m8_pr3country.dataBase.CountryEntity
import com.example.m7m8_pr3country.databinding.FragmentDetailBinding
import com.example.m7m8_pr3country.model.CountryItem
import com.example.m7m8_pr3country.viewModel.CountryViewModel
import com.squareup.picasso.Picasso
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class DetailFragment : Fragment() {
    lateinit var binding: FragmentDetailBinding
    private val viewModel: CountryViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        binding.detailImage.setImageResource()
        var country : CountryItem



        if(viewModel.fragmentFavorties){
            val favorite = viewModel.currentFavorite
            country = CountryItem(favorite.capital, favorite.currency, favorite.id.toInt(), favorite.media, favorite.name, favorite.population)
            binding.favoritesNo.visibility = INVISIBLE
            binding.favoritesYes.visibility = VISIBLE
        }else{
            var isFavorite = false
            country = viewModel.currentCountry
            for(favorite in viewModel.favorites.value!!){
                if(favorite.name == country.name) isFavorite = true
            }

            if(isFavorite){
                binding.favoritesNo.visibility = INVISIBLE
                binding.favoritesYes.visibility = VISIBLE
            }else{
                binding.favoritesNo.visibility = VISIBLE
                binding.favoritesYes.visibility = INVISIBLE
            }
        }






        if (!country.media.orthographic.isNullOrEmpty()) {
            Picasso.get().load(country.media.orthographic).into(binding.detailImage)
        }
        binding.name.text = country.name
        binding.capital.text = country.capital
        binding.population.text = country.population.toString()
        binding.currency.text = country.currency
        if (!country.media.flag.isNullOrEmpty()) {
            Picasso.get().load(country.media.flag).into(binding.flag)
        }


        binding.favoritesNo.setOnClickListener {
            binding.favoritesYes.visibility = View.VISIBLE
            binding.favoritesNo.visibility = View.INVISIBLE


            viewLifecycleOwner.lifecycleScope.launch {
                withContext(Dispatchers.IO) {
                    CountryApplication.database.favoritesDao().addCountry(
                        CountryEntity(
                            0,
                            country.capital,
                            country.currency,
                            country.media,
                            country.name,
                            country.population
                        )
                    )

                    val countries = CountryApplication.database.favoritesDao().getAllCountries()
                    viewModel.favorites.postValue(countries)

                }

            }
        }

        binding.favoritesYes.setOnClickListener{
            binding.favoritesYes.visibility = View.INVISIBLE
            binding.favoritesNo.visibility = View.VISIBLE

            viewLifecycleOwner.lifecycleScope.launch {
                withContext(Dispatchers.IO) {
                    CountryApplication.database.favoritesDao().deleteCountry(
                        CountryApplication.database.favoritesDao().getCountriesByName(country.name)
                    )

                    val countries = CountryApplication.database.favoritesDao().getAllCountries()
                    viewModel.favorites.postValue(countries)

                }

            }
        }



//        Glide.with(requireContext())
//            .load(viewModel.currentCountry.media.orthographic)
//            .diskCacheStrategy(DiskCacheStrategy.ALL)
//            .centerCrop()
//            .circleCrop()
//            .into(binding.detailImage)

    }
}

