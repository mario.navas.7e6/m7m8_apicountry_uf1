package com.example.m7m8_pr3country.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.m7m8_pr3country.R
import com.example.m7m8_pr3country.dataBase.CountryApplication
import com.example.m7m8_pr3country.dataBase.CountryEntity
import com.example.m7m8_pr3country.databinding.FragmentFavoritesBinding
import com.example.m7m8_pr3country.model.CountryItem
import com.example.m7m8_pr3country.utils.CountryAdapter
import com.example.m7m8_pr3country.utils.FavoritesAdapter
import com.example.m7m8_pr3country.utils.MyOnClickListener
import com.example.m7m8_pr3country.viewModel.CountryViewModel
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class FavoritesFragment : Fragment(), MyOnClickListener {
    lateinit var binding: FragmentFavoritesBinding
    private lateinit var favoriteAdapter: FavoritesAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentFavoritesBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModel = ViewModelProvider(requireActivity())[CountryViewModel::class.java]
        viewModel.fragmentFavorties = true

        viewLifecycleOwner.lifecycleScope.launch {
            withContext(Dispatchers.IO) {
                val countries = CountryApplication.database.favoritesDao().getAllCountries()
                viewModel.favorites.postValue(countries)
            }
        }

        viewModel.favorites.observe(viewLifecycleOwner){
            if (it != null) {
                setUpRecyclerView(it)
            }
        }
        binding.searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    if (query != "") {
                        val countries = viewModel.searchFavorite(query)
                        setUpRecyclerView(countries)
                    }
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    if (newText != "") {
                        val countries = viewModel.searchFavorite(newText)
                        setUpRecyclerView(countries)
                    }
                }
                return false
            }

        })
    }


    fun setUpRecyclerView(countries: MutableList<CountryEntity>){
        favoriteAdapter = FavoritesAdapter(countries, this)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerViewFavorites.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = favoriteAdapter
        }
    }

    override fun onClick(country: CountryItem) {
        TODO("Not yet implemented")
    }


    override fun onClickFavourite(country: CountryEntity) {
        val viewModel = ViewModelProvider(requireActivity())[CountryViewModel::class.java]
        viewModel.setSelectedFavortite(country)
        findNavController().navigate(R.id.action_favoritesFragment_to_detailFragment)
    }


}