package com.example.m7m8_pr3country.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.m7m8_pr3country.R
import com.example.m7m8_pr3country.dataBase.CountryEntity
import com.example.m7m8_pr3country.model.CountryItem
import com.example.m7m8_pr3country.databinding.FragmentRecyclerViewBinding
import com.example.m7m8_pr3country.utils.CountryAdapter
import com.example.m7m8_pr3country.utils.MyOnClickListener
import com.example.m7m8_pr3country.viewModel.CountryViewModel


class RecyclerViewFragment : Fragment(), MyOnClickListener {
    lateinit var binding: FragmentRecyclerViewBinding
    private lateinit var countryAdapter: CountryAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRecyclerViewBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModel = ViewModelProvider(requireActivity())[CountryViewModel::class.java]

        viewModel.fragmentFavorties = false
        viewModel.data.observe(viewLifecycleOwner){
            if (it != null) {
                setUpRecyclerView(it)
            }
        }
        binding.searchView.setOnQueryTextListener(object: SearchView.OnQueryTextListener{
            override fun onQueryTextSubmit(query: String?): Boolean {
                if (query != null) {
                    if (query != "") {
                        val countries = viewModel.searchCountry(query)
                        setUpRecyclerView(ArrayList(countries))
                    }
                }
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                if (newText != null) {
                    if (newText != "") {
                        val countries = viewModel.searchCountry(newText)
                        setUpRecyclerView(ArrayList(countries))
                    }
                }
                return false
            }

        })
    }


    fun setUpRecyclerView(countries: ArrayList<CountryItem>){
        countryAdapter = CountryAdapter(countries, this)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = countryAdapter
        }
    }

    override fun onClick(country: CountryItem) {
        val viewModel = ViewModelProvider(requireActivity())[CountryViewModel::class.java]
        viewModel.setSelectedCountry(country)
        findNavController().navigate(R.id.action_recyclerViewFragment_to_detailFragment)
    }

    override fun onClickFavourite(country: CountryEntity) {
        TODO("Not yet implemented")
    }
}

