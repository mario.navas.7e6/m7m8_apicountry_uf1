package com.example.m7m8_pr3country.model

data class CountryItem(
    val capital: String,
    val currency: String,
    val id: Int,
    val media: Media,
    val name: String,
    val population: Int,
    var favorite: Boolean = false
)
//     val abbreviation: String,     val phone: String,