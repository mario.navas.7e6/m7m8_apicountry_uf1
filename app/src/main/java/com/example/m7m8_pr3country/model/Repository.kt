package com.example.m7m8_pr3country.model

import com.example.m7m8_pr3country.api.ApiInterface


class Repository {
    val apiInterface = ApiInterface.create()

    suspend fun getCountries() = apiInterface.getCountries()
    suspend fun getCountry(id:Int) = apiInterface.getCountry(id)
}

