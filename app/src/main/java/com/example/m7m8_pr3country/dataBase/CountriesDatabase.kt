package com.example.m7m8_pr3country.dataBase

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters

@Database(entities = [CountryEntity::class], version = 1)
@TypeConverters(MediaConverter::class) // Add the MediaConverter here

abstract class CountriesDatabase: RoomDatabase() {
    abstract fun favoritesDao(): FavoritesDAO
}
