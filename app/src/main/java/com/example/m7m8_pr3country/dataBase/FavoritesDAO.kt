package com.example.m7m8_pr3country.dataBase

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.m7m8_pr3country.model.CountryItem

@Dao
interface FavoritesDAO {
    @Query("SELECT * FROM CountryEntity")
    fun getAllCountries(): MutableList<CountryEntity>
    @Query("SELECT * FROM CountryEntity where name = :countryName")
    fun getCountriesByName(countryName: String): CountryEntity
    @Insert
    fun addCountry(countryEntity: CountryEntity)
    @Update
    fun updateCountry(countryEntity: CountryEntity)
    @Delete
    fun deleteCountry(countryEntity: CountryEntity)

}