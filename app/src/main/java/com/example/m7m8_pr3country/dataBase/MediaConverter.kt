package com.example.m7m8_pr3country.dataBase

import androidx.room.TypeConverter
import com.example.m7m8_pr3country.model.Media
import com.google.gson.Gson

class MediaConverter {
    @TypeConverter
    fun fromMedia(media: Media?): String? {
        return media?.let { Gson().toJson(it) }
    }

    @TypeConverter
    fun toMedia(mediaString: String?): Media? {
        return mediaString?.let { Gson().fromJson(it, Media::class.java) }
    }
}