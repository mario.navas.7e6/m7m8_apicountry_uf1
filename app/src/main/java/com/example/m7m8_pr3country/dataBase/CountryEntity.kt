package com.example.m7m8_pr3country.dataBase

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.m7m8_pr3country.model.Media

@Entity(tableName = "CountryEntity")
data class CountryEntity(
    @PrimaryKey(autoGenerate = true) var id: Long = 0,
    val capital: String,
    val currency: String,
    val media: Media,
    val name: String,
    val population: Int
    )