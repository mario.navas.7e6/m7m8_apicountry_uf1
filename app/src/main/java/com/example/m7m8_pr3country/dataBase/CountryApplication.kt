package com.example.m7m8_pr3country.dataBase

import android.app.Application
import androidx.room.Room

class CountryApplication: Application() {
    companion object {
        lateinit var database: CountriesDatabase
    }
    override fun onCreate() {
        super.onCreate()
        database = Room.databaseBuilder(this,
            CountriesDatabase::class.java,
            "ContactDatabase").build()
    }

}