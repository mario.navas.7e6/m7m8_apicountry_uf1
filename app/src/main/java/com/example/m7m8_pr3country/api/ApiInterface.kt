package com.example.m7m8_pr3country.api

import com.example.m7m8_pr3country.model.Countries
import com.example.m7m8_pr3country.model.CountryItem
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {
    //Aquí posem les operacions GET,POST, PUT i DELETE vistes abans
    companion object {
        val BASE_URL = "https://api.sampleapis.com/countries/"
        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
    @GET("countries")
    suspend fun getCountries(): Response<Countries>

    @GET("country/{id}")
    suspend fun getCountry(@Path("id") characterId: Int): Response<CountryItem>
}
