package com.example.m7m8_pr3country.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.m7m8_pr3country.R
import com.example.m7m8_pr3country.dataBase.CountryEntity
import com.example.m7m8_pr3country.databinding.ItemCountryBinding
import com.example.m7m8_pr3country.model.CountryItem
import com.example.m7m8_pr3country.view.FavoritesFragment
import com.example.m7m8_pr3country.view.RecyclerViewFragment
import com.example.m7m8_pr3country.viewModel.CountryViewModel

class FavoritesAdapter (private val countries: List<CountryEntity>, private val listener: FavoritesFragment): RecyclerView.Adapter<FavoritesAdapter.ViewHolder>(){
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemCountryBinding.bind(view)
        fun setListener(country: CountryEntity){
            binding.root.setOnClickListener {
                listener.onClickFavourite(country)
            }
        }
    }
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_country, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val country = countries[position]
        with(holder) {
            setListener(country)
            binding.itemCountryText.text = country.name
            binding.itemCountryId.text = country.id.toString()
            context?.let {
                Glide.with(it)
                    .load(country.media.flag)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .circleCrop()
                    .into(binding.itemCountryImage)
            }
        }


    }

    override fun getItemCount(): Int {
        return countries.size
    }






}